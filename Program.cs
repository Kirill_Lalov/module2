using System;
using System.Collections.Generic;
using System.Globalization;
using System.Net;

namespace Module2
{
    public class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            Dimensions dimensions = new Dimensions();
            int figure;
            int parameter;
            int age;
            int companiesNumber;
            int tax;
            int companyRevenue;
            string first;
            string second;
            FigureEnum figureEnum;
            ParameterEnum parameterEnum;
            Console.WriteLine("Enter companies number");
            companiesNumber = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter the tax");
            tax = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Enter company revenue");
            companyRevenue = Convert.ToInt32(Console.ReadLine());
            program.GetTotalTax(companiesNumber, tax, companyRevenue);
            Console.WriteLine("Enter your age");
            age = Convert.ToInt32(Console.ReadLine());
            program.GetCongratulation(age);
            Console.WriteLine("Enter first number");
            first = Console.ReadLine();
            Console.WriteLine("Enter second number");
            second = Console.ReadLine();
            program.GetMultipliedNumbers(first, second);
            Console.WriteLine("Enter the figure ID");
            figure = Convert.ToInt32(Console.ReadLine());
            if (figure == 0)
            {
                figureEnum = FigureEnum.Triangle;
            }
            else if (figure == 1)
            {
                figureEnum = FigureEnum.Rectangle;
            }
            else if (figure == 2)  
            {
                figureEnum = FigureEnum.Circle;
            }
            else
            {
                figureEnum = 0;
            }
            Console.WriteLine("Enter the figure parameter");
            parameter = Convert.ToInt32(Console.ReadLine());
            if (parameter == 0) 
            {
                parameterEnum = ParameterEnum.Square;
            }
            else if (parameter == 1)
            {
                parameterEnum = ParameterEnum.Perimeter;
            }
            else
            {
                parameterEnum = 0;
            }
            program.GetFigureValues(figureEnum, parameterEnum, dimensions);
        }

        public int GetTotalTax(int companiesNumber, int tax, int companyRevenue)
        {
            int allTax;
            allTax = ((companiesNumber * companyRevenue) * tax) / 100;
            Console.WriteLine("All taxes are: {0}", allTax);
            return allTax;
        }

        public string GetCongratulation(int input)
        {
            var message = " ";
            if ((input % 2 == 0) && (input > 18) || (input == 18))
            {
                message = "���������� � ����������������!";
                Console.WriteLine(message);
                return message;
            }
            else if ((input % 2 != 0) && (input < 18) && (input > 12))
            {
                message = "���������� � ��������� � ������� �����!";
                Console.WriteLine(message);
                return message;
            }
            else
            {
                message = Convert.ToString(input);
                message = "���������� c " + message + " - ������!";
                Console.WriteLine(message);
                return message;
            }
        }

        public double GetMultipliedNumbers(string first, string second)
        {
            double firstNumber;
            double secondNumber;
            bool tryToConvert;
            double result;
            tryToConvert = double.TryParse(first, out result);
            if (tryToConvert == true)
            {
                firstNumber = double.Parse(first);
            }
            else
            {
                try
                {
                    IFormatProvider formatter = new NumberFormatInfo { NumberDecimalSeparator = "." };
                    firstNumber = double.Parse(first, formatter);
                }
                catch
                {
                    Console.WriteLine("You have entered letters ");
                    first = "0,00";
                }
                finally
                {
                    IFormatProvider formatter = new NumberFormatInfo { NumberDecimalSeparator = "." };
                    firstNumber = double.Parse(first, formatter);
                }
            }
            tryToConvert = double.TryParse(second, out result);
            if (tryToConvert == true)
            {
                secondNumber = double.Parse(second);
            }
            else
            {
                try
                {
                    IFormatProvider formatter = new NumberFormatInfo { NumberDecimalSeparator = "." };
                    secondNumber = double.Parse(second, formatter);
                }
                catch
                {
                    Console.WriteLine("You have entered letters ");
                    second = "0,00";
                }
                finally
                {
                    IFormatProvider formatter = new NumberFormatInfo { NumberDecimalSeparator = "." };
                    secondNumber = double.Parse(second, formatter);
                }
            }
            result = firstNumber * secondNumber;
            Console.WriteLine("{0} * {1} = {2}", firstNumber, secondNumber, result);
            return result;
        }

        public double GetFigureValues(FigureEnum figureType, ParameterEnum parameterToCompute, Dimensions dimensions)
        {
            double square;
            double perimeter;
            if ((figureType == FigureEnum.Triangle) && (parameterToCompute == ParameterEnum.Square))
            {
                Console.WriteLine("Enter the triangle side");
                dimensions.FirstSide = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Enter the triangle height");
                dimensions.Height = Convert.ToDouble(Console.ReadLine());
                square = dimensions.FirstSide * dimensions.Height;
                square = Math.Round(square);
                Console.WriteLine(square);
                return square;
            }
            else if ((figureType == FigureEnum.Triangle) && (parameterToCompute == ParameterEnum.Perimeter))
            {
                Console.WriteLine("Enter the first triangle side");
                dimensions.FirstSide = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Enter the second triangle side");
                dimensions.SecondSide = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Enter the third triangle side");
                dimensions.ThirdSide = Convert.ToDouble(Console.ReadLine());
                perimeter = dimensions.FirstSide + dimensions.SecondSide + dimensions.ThirdSide;
                perimeter = Math.Round(perimeter);
                Console.WriteLine(perimeter);
                return perimeter;
            }
            else if ((figureType == FigureEnum.Rectangle) && (parameterToCompute == ParameterEnum.Square))
            {
                Console.WriteLine("Enter the first rectangle side");
                dimensions.FirstSide = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Enter the second rectangle side");
                dimensions.SecondSide = Convert.ToDouble(Console.ReadLine());
                square = dimensions.FirstSide * dimensions.SecondSide;
                square = Math.Round(square);
                Console.WriteLine(square);
                return square;
            }
            else if ((figureType == FigureEnum.Rectangle) && (parameterToCompute == ParameterEnum.Perimeter))
            {
                Console.WriteLine("Enter the first rectangle side");
                dimensions.FirstSide = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Enter the second rectangle side");
                dimensions.SecondSide = Convert.ToDouble(Console.ReadLine());
                perimeter = 2 * (dimensions.FirstSide + dimensions.SecondSide);
                perimeter = Math.Round(perimeter);
                Console.WriteLine(perimeter);
                return perimeter;
            }
            else if ((figureType == FigureEnum.Circle) && (parameterToCompute == ParameterEnum.Square))
            {
                Console.WriteLine("Enter the circle radius");
                dimensions.Radius = Convert.ToDouble(Console.ReadLine());
                square = Math.PI * dimensions.Radius;
                square = Math.Round(square);
                Console.WriteLine(square);
                return square;
            }
            else if ((figureType == FigureEnum.Circle) && (parameterToCompute == ParameterEnum.Perimeter))
            {
                Console.WriteLine("Enter the circle diameter");
                dimensions.Diameter = Convert.ToDouble(Console.ReadLine());
                perimeter = Math.PI * dimensions.Diameter;
                perimeter = Math.Round(perimeter);
                Console.WriteLine(perimeter);
                return perimeter;
            }
            else
            {
                return 0.00;
            }
        }
    }
}

